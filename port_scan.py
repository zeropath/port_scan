#!/usr/bin/env python

import socket
import subprocess
import sys
from datetime import datetime

# Clear screen
subprocess.call('clear', shell=True)

# Input
remoteServer = input("Enter target:\n" )
remoteServerIP = socket.gethostbyname(remoteServer)

# Clear screen
subprocess.call('clear', shell=True)

# Banner
print("="*60)
print("Please wait, scanning " + remoteServerIP)
print("="*60)

# Start time
t1 = datetime.now()

# Using the range function to specify ports (here it will scans all ports between 1 and 1024)

# We also put in some error handling for catching errors

try:
    for port in range(1,1025):
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        result = sock.connect_ex((remoteServerIP, port))
        if result == 0:
            print("Port{}:  Open".format(port))
        sock.close()

except KeyboardInterrupt:
    print("You pressed Ctrl + C")
    sys.exit()

except socket.gaierror:
    print("Hostname could not be resolved. Exiting")
    sys.exit()

except socket.error:
    print("Couldn't connect to server")
    sys.exit()

# Checking the time again
t2 = datetime.now()

# Total time

t_total = t2-t1
print("Scanning Completed in: " +str(t_total))
